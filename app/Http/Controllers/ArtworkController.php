<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Artwork;
use Illuminate\Support\Facades\Session;

class ArtworkController extends Controller
{
    //


    public function create(Request $request)
    {

        // $this->validate($request, [
        // 'img' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1024'
        // ]);


      $artwork = new Artwork();

        if ($request->hasFile('img')) {
            $image = $request->file('img');
            $name = str_slug($request->title).'-'.str_slug($request->type).'-'.now()->timestamp.'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/images/artwork');
            $imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);
            $artwork->img = '/images/artwork/'.$name;
          }


        $artwork->title = $request->title;
        $artwork->type = $request->type;
        $artwork->desc = $request->desc;
        // $artwork->img = str_slug($request->img);

        $artwork->d_h = $request->d_h;
        $artwork->d_v = $request->d_v;
        $artwork->year = $request->year;
        $artwork->position = $request->position;

        $artwork->save();

        Session::flash('Success', 'Nice! you have a new Artwork in your gallery.!');
        return back();
    }



    public function update(Request $request, $id)
    {

        // $this->validate($request, [
        // 'img' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1024'
        // ]);


        $artwork = Artwork::findOrFail($id);

        if ($request->hasFile('img')) {
            $image = $request->file('img');
            $name = str_slug($request->title).'-'.str_slug($request->type).'-'.now()->timestamp.'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/images/artwork');
            $imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);
            $artwork->img = '/images/artwork/'.$name;
          }
          else
          {
            $artwork->img = $request->img_old;
          }


        $artwork->title = $request->title;
        $artwork->type = $request->type;
        $artwork->desc = $request->desc;
        // $artwork->img = str_slug($request->img);

        $artwork->d_h = $request->d_h;
        $artwork->d_v = $request->d_v;
        $artwork->year = $request->year;
        $artwork->position = $request->position;

        $artwork->save();

        Session::flash('Success', 'Updated! you updated an Artwork in your gallery.!');
        return back();
    }





    // public function create(Request $request)
    // {

    //     $artwork = new Artwork();
    //     $artwork->title = $request->title;
    //     $artwork->type = $request->type;
    //     $artwork->desc = $request->desc;
    //     $artwork->d_h = $request->d_h;
    //     $artwork->d_v = $request->d_v;
    //     $artwork->year = $request->year;
    //     $artwork->position = $request->position;
    //     $artwork->img = $request->img;
    //     $artwork->save();

    //     $article->image = $request->file('image')->storeAs(
    //         'my-folder-name', $article->slug
    //     );


    //     // return redirect('/');
    //     return back();

    // }




    public function view($id)
    {
        $artwork = Artwork::findOrFail($id);

        return view('artwork', [
            'artwork'=> $artwork
        ]);

    }



    public function editView($id)
    {
        $artwork = Artwork::findOrFail($id);

        return view('adm.adm-edit', [
            'artwork'=> $artwork
        ]);

    }


    public function deleteArtwork($id)
    {
        $artwork = Artwork::findOrFail($id);

        $artwork->delete();

        Session::flash('Suscess', 'You delete! an Artwork from your gallery!');
        return back();

    }





    public function listArtwork()
    {
        $artworks = Artwork::all();
        return view('adm.adm-list', [
            'artworks' => $artworks
        ]);
    }


    public function listArtworkHome()
    {
        $artworks = Artwork::all();
        return view('home', [
            'artworks' => $artworks
        ]);
    }





}
