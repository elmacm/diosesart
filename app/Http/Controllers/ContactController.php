<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use Session;

class ContactController extends Controller
{
    //


    public function postContact(Request $request) {

        $this->validate($request, [
            'inquire_name' => 'min:3',
            'inquire_email' => 'required|email',
            'inquire_message' => 'min:10'
        ]);

        $data = array(
            'inquire_name' => $request->inquire_name,
            'inquire_email' => $request->inquire_email,
            'inquire_message' => $request->inquire_message
        );

        Mail::send('emails.contact', $data, function ($message)  use ($data) {
            $message->from($data['inquire_email'], $data['inquire_name']);
            $message->to('camamanuel@gmail.com', 'Manuel');
            $message->subject('New Contact');
            $message->priority(3);
        });

        Session::flash('sucess', 'Your Email was sent!');

        return redirect(url('contact'));


    }


    public function postInquire(Request $request) {

        $this->validate($request, [
            'inquire_name' => 'min:3',
            'inquire_email' => 'required|email',
            'inquire_message' => 'min:10',
            'inquire_product' => 'min:1',
            'inquire_product_url' => 'min:1'

            ]);

        $data = array(
            'inquire_name' => $request->inquire_name,
            'inquire_email' => $request->inquire_email,
            'inquire_message' => $request->inquire_message,
            'inquire_product' => $request->inquire_product,
            'inquire_product_url' => $request->inquire_product_url
         );

        Mail::send('emails.contact', $data, function ($message)  use ($data) {
            $message->from($data['inquire_email'], $data['inquire_name']);
            $message->to('camamanuel@gmail.com', 'Manuel');
            $message->subject('New Inquire');
            $message->priority(3);
        });

        Session::flash('sucess_inquire', 'Your Inquire was sent!');
        // return redirect(url('art'));
        return back();
    }



}
