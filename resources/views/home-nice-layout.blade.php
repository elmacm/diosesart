@extends('main')

 @section('title', 'Dioses Art Gallery - Surrealism and Contemporary Art oil paintings, photography, drawings, fine art' )

 @section('scripts_metas')

 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 <script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.js""></script>
 <script src=" https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.js""> </script> <script>
     jQuery(document).ready(function () {
         // init Masonry
         var $grid = jQuery('.grid').masonry({
             itemSelector: '.grid-item',
             percentPosition: true,
             columnWidth: '.grid-sizer'
         });
         // layout Masonry after each image loads
         $grid.imagesLoaded().progress(function () {
             $grid.masonry();
         });

     });
 </script>
@endsection


 @section('content')
 <div class="m-5">
     <p class="text-justify"><strong>Dioses Art</strong> is a fine art gallery. We specialize in representational works,
         with a concentration on surrealism and contemporary art. oil paintings, photography, drawings, fine art.</p>
 </div>



 <div class="grid row">
     <div class="grid-sizer "></div>

     @foreach ($artworks as $artwork)
     <div class="grid-item">
            <a href="/artwork/{{ $artwork->id }}">
                <h2 class=" text-center">{{ $artwork->title }}</h2>
                <img src="{{ $artwork->img }}" />
                <p class="text-right">
                    <a class="view-details" href="/artwork/{{ $artwork->id }}">view details / inquire price  <i class="fas fa-caret-right"></i></a>
                </p>
            </a>
    </div>

     @endforeach
     <div class="grid-item">
         <a href="">
             <h2 class=" text-center">Elohim</h2>
             <img src="{{ asset('img/art/elohim-by-dioses-art.jpg') }}" />
             <p class="text-right"><a class="view-details" href="/id/slug">view details / inquire price <i
                         class="fas fa-caret-right"></i></a></p>
         </a>
     </div>
     <div class="grid-item">
         <a href="">
             <h2 class=" text-center">Emmanuel</h2>
             <img src="{{ asset('img/art/emmanuel-by-dioses-art.jpg') }}" />
             <p class="text-right"><a class="view-details" href="/id/slug">view details / inquire price <i
                         class="fas fa-caret-right"></i></a></p>
         </a>

     </div>
     <div class="grid-item ">
         <img src="{{ asset('img/art/elohim-III-by-dioses-art.jpg') }}" />
     </div>
     <div class="grid-item">
         <img src="{{ asset('img/art/adonay-by-dioses-art.jpg') }}" />
     </div>
     <div class="grid-item">
         <img src="{{ asset('img/art/cuba-by-dioses-art.jpg') }}" />
     </div>
     <div class="grid-item">
         <img src="{{ asset('img/art/habana-by-dioses-art.jpg') }}" />
     </div>

 </div>

 @endsection
