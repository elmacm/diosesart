@extends('main')

@section('title', 'ArtName - by Dioses Art Gallery : DiosesArt.com')

@section('scripts_metas')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
@endsection


@section('content')

<div class="row">
<div class="col-md-4">
    <h1 class="text-left">Elohim</h1>
    <div class="art-desc">
            <p>Type: oil and 23k gold leaf on canvas</p>
            <p>Dimensions: 22 x 30 in</p>
            <p>Year: ≈2000</h2>
    </div>


    @if(Session::has('sucess_inquire'))
        <div class="alert alert-info">
            <a class="close" data-dismiss="alert">×</a>
            <strong>Sucess:</strong> {!!Session::get('sucess_inquire')!!}
        </div>
    @endif




            <p><button class="btn btn-dark" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                    <i class="far fa-file-alt"></i> Inquire Price
            </button></p>

            <div class="collapse" id="collapseExample">
            <div class="card card-body">

                    <form method="POST" action="{{ url('art') }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                    <input type="text" class="form-control" name="inquire_name" id="inquire_name" placeholder="Your Full Name" required>
                                </div>

                                <div class="form-group">
                                    <input type="email" class="form-control" name="inquire_email" id="inquire_email" placeholder="your@email.com" required>
                                </div>

                                <div class="form-group">
                                    <textarea class="form-control" name="inquire_message" id="inquire_message" placeholder="Message, please include your shipping address, best time to contact and phone number" rows="3" required></textarea>
                                </div>

                                <button class="btn btn-dark" type="submit"><i class="far fa-paper-plane"></i> Send</button>
                        </form>




            </div>
            </div>


</div>
<div class="col-md-8 text-center">
    <img class="img-fluid" src="{{ asset('img/art/elohim-by-dioses-art.jpg') }}" />
</div>
</div>


@endsection
