<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>@yield('title')</title>

    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

        @yield('scripts_metas')

</head>

<body class="">
        @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
          </ul>
        </div><br />
      @endif

    <div class="container dsa-content">

    <header >
        <div class="text-center my-4">
            <h1><a title="Dioses Art" href="/"><img src="{{ asset('img/logo-DiosesART.svg') }}" alt="Dioses Art" /></a></h1>
        </div>

        {{-- <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Data</li>
            </ol>
        </nav> --}}

    </header>
    <hr class="my-5"/>



        <main class="container">
            @yield('content')
        </main>


        <hr class="footer-line mt-5">

        <footer class="text-center mt-0">

            <ul class="list-inline social-icons text-center">
                <li class="list-inline-item"><a href=""><i class="fab fa-instagram"></i></a></li>
                <li class="list-inline-item"><a href=""><i class="fab fa-pinterest"></i></a></li>
                <li class="list-inline-item"><a href="/contact"><i class="far fa-envelope"></i></a></li>
            </ul>

            <ul class="list-inline text-right footer-links">
                <li class="list-inline-item"><a href="">Terms of Service</a></li>
                <li class="list-inline-item"><a href="">Privacy Police</a></li>
                <li class="list-inline-item"><a href="">Contact Us</a></li>
                <li class="list-inline-item">Copyright ® 2019 - Dioses ART LLC</li>
            </ul>
        </footer>

    </div>
</body>
</html>
