@extends('adm.adm-main')

@section('title', 'ADMIN - Dioses Art Gallery - DiosesArt.com')

@section('scripts_metas')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
@endsection


@section('content')

<form method="POST" action="{{ url('adm/add') }}" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="form-group">
        <input type="text" class="form-control" name="title" id="title" placeholder="Artwork Name" required>
    </div>

    <div>
        <input type="file" name="img" id="img" placeholder="Artwork Name" required/>
    </div>

    <div class="form-group">
        <input type="text" class="form-control" name="type" id="type" placeholder="Artwork Type eg: Oleo, Photography..." required>
    </div>

    <div class="form-group">
        <textarea class="form-control" name="desc" id="desc" placeholder="A full Description of the Artwork" rows="3" ></textarea>
    </div>

    <div class="form-group">
        <input type="number" class="form-control" name="d_h" id="d_h" placeholder="Artwork Width in inches " required>
    </div>


    <div class="form-group">
       <input type="number" class="form-control" name="d_v" id="d_v" placeholder="Artwork Vertical in inches " required>
    </div>

    <div class="form-group">
        <input type="text" class="form-control" name="year" id="year" placeholder="Artwork Year" >
    </div>

    <div class="form-group">
        <input type="number" class="form-control" name="position" id="position" placeholder="Artwork position" >
    </div>


    <button class="btn btn-dark" type="submit"><i class="fas fa-plus"></i> Create Artwork</button>
</form>




@endsection
