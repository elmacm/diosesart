@extends('adm.adm-main')


@section('scripts_metas')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
@endsection


@section('content')
<h1>Admin Area</h1>
<hr/>

<h2 class="my-5">Artwork:</h2>
<a href={{ route('adm.add') }} class="btn btn-primary">Add</a>
<a href="{{ route('adm.list') }}"  class="btn btn-primary">Edit/Remove</a>
@endsection
