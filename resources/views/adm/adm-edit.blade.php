@extends('adm.adm-main')


@section('scripts_metas')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
@endsection


@section('content')

<form method="POST" action="{{action('ArtworkController@update', $artwork->id)}}" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="form-group">
        <input type="text" class="form-control" name="title" id="title"  value="{{ $artwork->title }}"    required>
    </div>

    <div>
        <label for="type">Update Image </label>
        <p><input type="file" name="img" id="img" /> <br/><small>Only if you want to upload a new image</small></p>
        <p>Current Image <img src="{{ $artwork->img }}" width="30px"> </p>
        <input type="hidden" class="form-control" name="img_old" id="img_old" value="{{ $artwork->img }}" required>
        <input type="text" class="form-control" name="id" id="id" value="{{ $artwork->id }}" required>

    </div>

    <div class="form-group">
        <label for="type">Type of Artwork</label>
        <input type="text" class="form-control" name="type" id="type" value="{{ $artwork->type }}"  required>
    </div>

    <div class="form-group">
        <label for="desc">Description</label>
        <textarea class="form-control" name="desc" id="desc" rows="3" >{{ $artwork->desc }}</textarea>
    </div>

    <div class="form-group">
        <label for="d_h">Width (inch)</label>
        <input type="number" class="form-control" name="d_h" id="d_h"  value="{{ $artwork->d_h }}" required>
    </div>


    <div class="form-group">
        <label for="d_v">Height (inch)</label>
        <input type="number" class="form-control" name="d_v" id="d_v"  value="{{ $artwork->d_v }}"  required>
    </div>

    <div class="form-group">
        <label for="year">Year</label>
        <input type="text" class="form-control" name="year" id="year"  value="{{ $artwork->year }}"  >
    </div>

    <div class="form-group">
        <label for="position">Position</label>
        <input type="number" class="form-control" name="position" id="position"   value="{{ $artwork->position }}"  >
    </div>


    <button class="btn btn-dark" type="submit"><i class="far fa-edit"></i> Update</button>
</form>



@endsection
