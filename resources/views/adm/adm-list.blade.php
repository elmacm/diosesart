@extends('adm.adm-main')


@section('scripts_metas')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script>
$(document).ready( function () {
    $('#list-artwork').DataTable();
} );
</script>


<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"/>>

@endsection


@section('content')
<h1></h1>




<table id="list-artwork" class="table table-responsive table-striped">
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Name</th>
            <th scope="col">Image</th>
            <th scope="col">Type</th>
            <th scope="col">Description</th>
            <th scope="col">Width</th>
            <th scope="col">Height</th>
            <th scope="col">Year</th>
            <th scope="col">Actions</th>

          </tr>
        </thead>
        <tbody>
        @foreach ($artworks as $artwork)
        <tr>
            <th scope="row">{{$artwork->id}}</th>
            <td>{{$artwork->title}}</td>
            <td><img  height="60px" src="{{$artwork->img}}" alt="{{$artwork->title}}" /></td>
            <td>{{$artwork->type}}</td>
            <td>{{$artwork->desc}}</td>
            <td>{{$artwork->d_h}}</td>
            <td>{{$artwork->d_v}}</td>
            <td>{{$artwork->year}}</td>
            <td class="text-center">
                <a title="View"  target="_blank" class="btn btn-success btn-sm" href="/artwork/{{$artwork->id}}"><i class="fas fa-eye"></i></a>
                <a title="Edit" class="btn btn-warning btn-sm" href="/adm/edit/{{$artwork->id}}"><i class="fas fa-edit"></i></a> <br/><br/>
                <a title="DELETE!" class="btn btn-danger btn-sm" href="/adm/del/{{$artwork->id}}"><i class="fas fa-minus-circle"></i></a>
            </td>
        </tr>

        @endforeach
        </tbody>
      </table>


@endsection
