@extends('main')

@section('title', 'Contact Us')

@section('content')

    <h1 class="my-5">Contact us</h1>

    @if(Session::has('sucess'))
        <div class="alert alert-info">
            <a class="close" data-dismiss="alert">×</a>
            <strong>Sucess:</strong> {!!Session::get('sucess')!!}
        </div>
    @endif



    <form method="POST" action="{{ url('contact') }}">
        {{ csrf_field() }}
        <div class="form-group">
                <input type="text" class="form-control" name="inquire_name" id="inquire_name" placeholder="Your Full Name" required>
            </div>

            <div class="form-group">
                <input type="email" class="form-control" name="inquire_email" id="inquire_email" placeholder="your@email.com" required>
            </div>


            <div class="form-group">
                <textarea class="form-control" name="inquire_message" id="inquire_message" placeholder="Message, please include your shipping address, best time to contact and phone number" rows="3" required></textarea>
            </div>

            <button class="btn btn-dark" type="submit"><i class="far fa-paper-plane"></i> Send</button>
    </form>



 @endsection
