@extends('main')

@section('title', $artwork->title.' by Dioses Art Gallery : DiosesArt.com')

@section('scripts_metas')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
@endsection


@section('content')

<div class="row">
<div class="col-md-4">
    <h1 class="text-left"> {{ $artwork->title }}</h1>
    <div class="art-desc">
            <p class="art-type">{{ $artwork->type }}</p>
            <p>{{ $artwork->d_h }} x {{ $artwork->d_v }} inch <br/> ({{ $artwork->d_h*2.54 }} x {{ $artwork->d_v*2.54 }} cm)</p>
            <p> ≈{{ $artwork->year }}</h2>
    </div>


    @if(Session::has('sucess_inquire'))
        <div class="alert alert-info">
            <a class="close" data-dismiss="alert">×</a>
            <strong>Sucess:</strong> {!!Session::get('sucess_inquire')!!}
        </div>
    @endif

        <hr/>

            <p><button class="btn btn-dark" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                    <i class="far fa-file-alt"></i> Inquire Price
            </button></p>

            <div class="collapse" id="collapseExample">
            <div class="card card-body">

                    <form method="POST" action="{{ url('artwork') }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                    <input type="text" class="form-control" name="inquire_name" id="inquire_name" placeholder="Your Full Name" required>
                                </div>

                                <div class="form-group">
                                    <input type="email" class="form-control" name="inquire_email" id="inquire_email" placeholder="your@email.com" required>
                                </div>

                                <div class="form-group">
                                    <textarea class="form-control" name="inquire_message" id="inquire_message" placeholder="Message, please include your shipping address, best time to contact and phone number" rows="3" required></textarea>
                                </div>

                                <div class="form-group">
                                        <input type="hidden" class="form-control" name="inquire_product" id="inquire_product" value="{{ $artwork->title }}" required>
                                        <input type="hidden" class="form-control" name="inquire_product_url" id="inquire_product_url" value="{{ Request::fullUrl() }}" required>
                                </div>
                             <button class="btn btn-dark" type="submit"><i class="far fa-paper-plane"></i> Send</button>
                    </form>


            </div>
            </div>


</div>
<div class="col-md-8 text-center">
    <img class="img-fluid" src="{{$artwork->img}}" alt="{{$artwork->title}}" />
</div>
</div>


@endsection
