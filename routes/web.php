<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ArtworkController@listArtworkHome');



// Route::get('/', function () {
//     return view('home');
// });


Route::get('artwork/{id}', 'ArtworkController@view');

Route::post('/artwork', 'ContactController@postInquire');


// Contact
Route::get('/contact', function () {
    return view('contact');
});

Route::post('contact', 'ContactController@postContact');



// ADM

Route::get('/adm', function () {
    return view('adm.adm-home');
});

Route::get('/adm/add', function () {
    return view('adm.adm-add');
})->name('adm.add');

Route::post('adm/add', 'ArtworkController@create');




// Edit
Route::get('adm/edit/{id}', 'ArtworkController@editView');
Route::post('adm/edit/{id}', 'ArtworkController@update');





// DElete
Route::get('adm/del/{id}', 'ArtworkController@deleteArtwork');



// List all Artwork
// Route::get('adm/list', function () {
//     return view('adm.adm-list');
// })->name('adm.list');

Route::get('adm/list', 'ArtworkController@listArtwork')->name('adm.list');




Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
